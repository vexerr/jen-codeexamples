﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace code_examples {
    public class Configuration {
        public static string Issuer = "issuer";
        public static string Audience = "audience";
        public static string Key = "key";
    }

    public enum Operations {
        Create,
        Read,
        Update,
        Delete
    }
    
    public class Accessor {
        public bool ClaimCanAccess(ClaimsPrincipal claim, Operations op) {
            if (claim.Identity.Name == "admin") {
                return true;
            }
            return false;
        }
        
        public bool CanAccess(string token, Operations op) {
            var handler = new JwtSecurityTokenHandler();
            var tokenResults = handler.ValidateToken(
                token,
                new TokenValidationParameters {
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.Key))
                },
                out var validatedToken
            );

            return ClaimCanAccess(tokenResults, op);

        }
    }
}
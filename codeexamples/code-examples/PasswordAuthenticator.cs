﻿using System;
using System.Reflection;

namespace code_examples {
    public class UserService {
        public bool CanSignIn(string username, string password) {
            return true;
        }
    }
    public class InvalidPasswordException : Exception {
        public InvalidPasswordException(string msg) : base(msg) { }
    }

    public class PasswordAuthenticator {
        public bool CanAuthenticate(
            string username, 
            string password) {
            // method calls a user service to verify user/password combination
            return new UserService().CanSignIn(username, password);
        }

        public void PasswordProtect(
            string username, 
            string password, 
            Action actionToDo) {
            if (!this.CanAuthenticate(username, password))
                // method exits when this exception is thrown
                throw new InvalidPasswordException(
                    $"Password denied for {username}");

            actionToDo.Invoke();
        }
    }
}
﻿using System;

namespace code_examples {
    public class PartOfHome { }

    public class Outside : PartOfHome { }

    public class Yard : Outside {
        public void Mow() {
            Console.WriteLine("mow");
        }
    }

    public class Inside : PartOfHome { }

    public class Bedroom : Inside {
        public void Vacuum() {
            Console.WriteLine("vacuum");
        }
    }

    public class Kitchen : Inside {
        public void Mop() {
            Console.WriteLine("mop");
        }
    }


    public class DoingWork {

        public void DoWork(PartOfHome partOfHome) {
            switch (partOfHome) {
                case Yard yard:
                    yard.Mow();
                    break;
                case Bedroom bedroom:
                    bedroom.Vacuum();
                    break;
            }
        }
    }
}
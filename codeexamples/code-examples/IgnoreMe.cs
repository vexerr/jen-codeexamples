using System;

namespace code_examples {
    public class IgnoreMe {
        public void MakingSureStuffIsReferenced() {
            var accessor = new Accessor();
            accessor.CanAccess(String.Empty, Operations.Create);
            accessor.CanAccess(String.Empty, Operations.Read);
            accessor.CanAccess(String.Empty, Operations.Update);
            accessor.CanAccess(String.Empty, Operations.Delete);
            
            
            var password = new PasswordAuthenticator();
            password.PasswordProtect("user", "password", () => Console.WriteLine("action"));
            
            
            var work = new DoingWork();
            work.DoWork(new Bedroom());
            work.DoWork(new Kitchen());
            work.DoWork(new Outside());
            work.DoWork(new Inside());
            work.DoWork(new Outside());
            
        }
    }
}